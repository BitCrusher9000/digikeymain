/*******************************************************************************
  Microstick Main File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    The main file for the SPI interface experiment

  Description:
    This file contains the application logic for using the SPI interface

*******************************************************************************/
// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*******************************************************************************/
// DOM-IGNORE-END

// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************
/*  This section lists the other files that are included in this file. */

#include <stdint.h>
#include <xc.h>
#include "main.h"
#include <p24F16KL402.h>
#include <stdio.h>

//Section: function declaration
/*Declare functions*/
void delayMs(unsigned int);
void delayUs(unsigned int);
void driveMotor(int, unsigned int, int);
void easeIn(int, int);
void easeOut(int, int);
/*******************/

/* Clock division equation process
 Clock divider = Fosc/2
 Pre-scaler = 8
 Tick Counter Frequency = 8/4E6 = 2us
 Timer Count MS = 10ms/2us = 20000 = 0x1388
 Timer Count US = 10us/2us = 5 = 0x5
 */
 
//Section: Define global variables as constants to use throughout file
#define DELAYMS 0x1388
#define DELAYUS 0x5
#define STEP_PIN LATBbits.LATB15
#define DIR_PIN LATBbits.LATB14

//Section: Define other global variables to this file with types
int steps;
int topAcceleration = 10;
int botAcceleration = 20;
//Section: Start the main setup for the program
int main() 
{
    //Section: Define setup for all pin modes that the microchip supports and other configurations
    /*Define pin outputs for appropriate pins connected to the sparkfun*/
    TRISBbits.TRISB15 = 0; //output for STEP_PIN
    TRISBbits.TRISB14 = 0; //output for DIR_PIN
    /*******************************************************************/
    
    /*Set up the timer for delay*/
    T1CONbits.TCS = 0; //sets timer frq to Fosc/2 4Mhz
    T1CONbits.TCKPS = 01; //sets prescaler to 8
    T1CONbits.TON = 0; //make sure the timer is off from the start
    /****************************/
    
    //Section: Code execution, could be a while(1), but don't need to
    easeIn(botAcceleration, topAcceleration);
    driveMotor(2000, 1, topAcceleration);
    easeOut(topAcceleration, botAcceleration + 10);
    return 0;
}

//Section: Function Definition
/*There should be an easier guide in speed instead of starting at top speed
 This function defines start up acceleration so that the motor can ease into
 top speed. The function only accepts two parameters: int botaccel and int topaccel
 These can really be any number, however, it was found that using 20 as a bottom
 acceleration and 10 as a top acceleration works pretty well. */
void easeIn(int botaccel, int topaccel){
    //use a for loop to build up to speed
    //bot accel and top accel are defined as a multiple of 10us
    //if you input 20, the microsecond delay will be 200us
    //the testing statement for the loop will be hitting the maximum acceleration when you call the function I.E. >= topaccel
    //the bottom acceleration will increase by decreasing the amount of delay
    for(botaccel; botaccel >= topaccel; botaccel--){
        //drive the motor an arbitrary amount of steps, in one direction, and use botaccel as this is the number that is changing in the for loop
        driveMotor(500, 1, botaccel);
        //put slight delay between the next execution of the for loop for more obvious acceleration, 10us will be fine
        delayUs(1);
    }
}
//Example of use: easeIn(20, 10);
//This will start with a delay of 200us between step instructions and decrease down to 100us of delay using the for loop functionality

/*This function is the reverse operation of the previous function, it 
 eases the motor to a halt*/
void easeOut(int topaccel, int botaccel){
    for(topaccel; topaccel <= botaccel; topaccel++){
        driveMotor(500, 1, topaccel);
        delayUs(1);
    }
}
//Example of use: easeOut(10, 20);
//This will bring the motor from using 100us between step instructions down to 200us, because the file ends after that, the motor will stop

/*The function below is repeatable to drive the motor whatever way we want repeatedly*
 *the return type is void because we are not using any data that comes out, there are*
 *four parameters you can put values into. int step: how many steps do you want the  *
 *motor to take?, unsigned int direction: what direction do you want the motor to go?*
 *(0 drives it one way, 1 drives it the other way), int delayMicro: how many         *
 *multiples of 10us you want for the delay time between steps (less = faster, more   *
 * = slower) CAREFUL: going too fast will make the motor heat up quicker due to fast *
 *change in current!                                                                 */
void driveMotor(int step, unsigned int direction, int delayMicro){
  DIR_PIN = direction; //set direction of motor
  steps = step; //set the number of steps to take
  while(steps >= 0){ //wait for steps to go all the way down to zero
    STEP_PIN = 0; 
    delayUs(delayMicro); //delay a multiple of 10us for quick transition
    STEP_PIN = 1; //going from low to high will step the motor
    delayUs(delayMicro);
    steps--; //subtract one from steps each iteration
  }
}
//Example of use: driveMotor(50, 0, 20); 
//will set the number of steps to 50, drive the motor direction associated with logic LOW, and delay 200us between each change of logic

//The function below works the same way as delayMS, except it uses a multiple of 10us
void delayUs(unsigned int xUS){
    TMR1 = 0;
    T1CONbits.TON = 1;
    int c1;
    for(c1 = 0; c1 < xUS; c1++){
        TMR1 = 0;
        while(TMR1 < DELAYUS);
    }
    T1CONbits.TON = 0;
}
//Example of use: delayUs(10);
//Will delay the instruction by 10 * 10us = 100us (microseconds)

/*The function below uses the built in Timer1 register to put some delay between code execution, *
 *there is an explanation at the top of this file how the number 10ms = 0x1388 using specific    *
 *values set by the register T1CON, the function returns no values and only delays the program   *
 *instructions by multiples of 10, because 10 is easier to deal with. Only one parameter is used:*
 *xMS, this defines how many multiples of 10ms you want (i.e. 1 will be 10ms, 5 will be 50ms, 10 *
 *will be 100ms, etc...) The method for timing sets a pre-defined variable called TMR1 to 0,     *
 *starts the timer, iterates waiting for TMR1 to reach DELAYMS x number of xMS times, and finally*
 *turn off the timer when finished. The function always makes sure that TMR1 is set back to 0 at *
 *appropriate times so we don't reference some random value and xMS.                             */
void delayMs(unsigned int xMS){
    //make sure TMR1 is cleared
    TMR1 = 0;
    //Start the timer
    T1CONbits.TON = 1;
    int c1;
    //iterate the "wait" code xMS number of times
    for(c1 = 0; c1 <= xMS; c1++){
        //we really need to be sure TMR1 resets to 0 when it reaches the max we set
        TMR1 = 0;
        //wait for TMR1 to reach the DELAY bit
        while(TMR1 < DELAYMS);
    }
    //timer should be turned off after all use is complete
    T1CONbits.TON = 0;
}
//Example of use: delayMs(50);
//Will delay instruciton by 50 * 10ms, which is 500ms or half a second