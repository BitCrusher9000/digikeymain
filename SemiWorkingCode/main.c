/*******************************************************************************
  Microstick Main File
  Company:
    Microchip Technology Inc.
  File Name:
    main.c
  Summary:
    The main file for the SPI interface experiment
  Description:
    This file contains the application logic for using the SPI interface
*******************************************************************************/
// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013 released Microchip Technology Inc.  All rights reserved.
Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).
You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.
SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*******************************************************************************/
// DOM-IGNORE-END
// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************
/*  This section lists the other files that are included in this file. */
#include <stdint.h>
#include <xc.h>
#include "main.h"
#include <p24F16KL402.h>
#include <stdio.h>
//Section: function declaration
void delayMs(int);
void delayUs(int);
void driveMotor(int, unsigned int, int);
void easeOut(int, int);
unsigned int readAnalogChannel(unsigned int);
/* Clock division equation process
 Clock divider = Fosc/2; Fosc/Pre-DefinedNumber->Generic Form:: The Pre-Defined Numbers are available in the T1CON control register in the data sheet
 Pre-scaler = 8; Pre-scaler = PS where PS is a number->Generic Form
 Tick Counter Frequency = 8/4E6 = 2us; TCF = PS/(Fosc/#)->Generic Form
 Timer Count MS = 10ms/2us = 20000 = 0x1388; Desired Delay = NumberofSeconds/TCF convert to HEX
 Timer Count US = 10us/2us = 5 = 0x5
 NOTE: Keep in mind there will be maximum values based on your Fosc/# and prescaler selection, limited from 0 to 65535 due to 16 bit limitation on the databus
 */
 
//Section: Define global variables as constants to use throughout file
#define DELAYMS 0x1388 //10 ms of delay calculated off 8 Pre-Scaler and 8Mhz
#define DELAYUS 0x5 //10 us of delay calculated off 8 Pre-Scaler and 8Mhz
#define STEP_PIN LATBbits.LATB15 //define the step pin for Easy Driver
#define DIR_PIN LATBbits.LATB14 //define the direction pin for Easy Driver
#define LED_C LATBbits.LATB6 //LED correct definition
#define LED_WE LATBbits.LATB7 //LED Wrong End definition
#define LED_UD LATBbits.LATB8 //LED Upsided Down definition
//Section: Define other global variables to this file with types
int topAcceleration = 10;
int botAcceleration = 20;
int dir = 0;
unsigned int stl; //lower case for storage of data for top left sensor
unsigned int sbl; //lower case for storage of data for bottom left sensor
unsigned int str; //lower case for storage of data for top right sensor
unsigned int sbr; //lower case for storage of data for bottom right sensor
//Section: Start the main setup for the program
int main() 
{
    //Define the channel hexadecimal addresses
    unsigned int STL = 0x0000; //AN2 channel address ORANGE
    unsigned int SBL = 0x0003; //AN3 channel address YELLOW
    unsigned int STR = 0x0004; //AN4 channel address RED
    unsigned int SBR = 0x000D; //AN13 channel address BLUE
    //Section: Define setup for all pin modes that the microchip supports and other configurations
    /*Define pin outputs for appropriate pins connected to the sparkfun*/
    TRISBbits.TRISB15 = 0; //output for STEP_PIN
    TRISBbits.TRISB14 = 0; //output for DIR_PIN
    
    TRISAbits.TRISA0 = 1;
    ANSAbits.ANSA0 = 1;
    
    TRISBbits.TRISB1 = 1; //input for RB1 (need for analog)
    ANSBbits.ANSB1 = 1; //sets up analog for RB1
    
    TRISBbits.TRISB2 = 1; //set RB2 as input
    ANSBbits.ANSB2 = 1; //set RB2 as analog
    
    TRISAbits.TRISA2 = 1; //set RA2 as input
    ANSAbits.ANSA2 = 1; //set RA2 as analog
    
    TRISBbits.TRISB6 = 0; //Sets digital pin RB6 as an output (LED Output for Testing)
    TRISBbits.TRISB7 = 0; //set digital pin RB7 output
    TRISBbits.TRISB8 = 0; //set digital pin RB8 output
    
    //Sub-Section: Analog Initialization Control Registers
    AD1CON1 = 0x2000; //initialize all values of AD1CON1 (use programming calculator for full binary sequence [see datasheet for equivalent settings])
    AD1CON2 = 0; //sets all AD1CON2 bits to 0, refer to datasheet to see what that means for that control register
    AD1CON3 = 0x0002; //sets AD1CON3, (programming calculator for full binary sequence [see datasheet for equivalent settings])
    AD1CHSbits.CH0NA = 0;
    
    /*Set up the timer for delay*/
    T1CONbits.TCS = 0; //sets timer frq to Fosc/2 4Mhz
    T1CONbits.TCKPS = 01; //sets prescaler to 8
    T1CONbits.TON = 0; //make sure the timer is off from the start
    
    //Turn the analog to digital converter on
    AD1CON1bits.ADON = 1;
    int count = 0;
    while(1){
        stl = readAnalogChannel(STL); //orange sensor
        sbl = readAnalogChannel(SBL); //yellow sensor
        str = readAnalogChannel(STR); //red sensor
        sbr = readAnalogChannel(SBR); //blue sensor
        if(stl > 85 && sbl > stl){
            LED_C = 1;
            driveMotor(5000, dir, botAcceleration);
            delayUs(5);
        } else if(str > 80){
            LED_C = 0;
            LED_WE = 1;
        } else if(sbr > 110) {
            LED_C = 0;
            LED_WE = 0;
            LED_UD = 1;
        } else if(sbl > 114){
            LED_C = 0;
            LED_WE = 1;
            LED_UD = 1;
        } else {
            LED_C = 0;
            LED_WE = 0;
            LED_UD = 0;
        }
        
    }
    return 0;
}
//Section: Function Definition
/*This function is designed to take analog measurements from specific channels*
 *one at a time. The return type is an unsigned integer because ADC1BUF0 is an*
 *unsigned integer and the channel for Multiplexer A is also an unsigned int  *
 *The only parameter is the channel which tells the A/D converter to sample   *
 *hold the analog information. It was concluded that there are two methods to *
 *sampling several channels: Alternate between Multiplexers A and B or simply *
 *use one channel and delay between different channels being sampled. This    *
 *function is designed around the second method, using one Multiplexer A.     */
unsigned int readAnalogChannel(unsigned int channel){
    //set up a local variable to store the ADC Buffer into (use as a return value)
    unsigned int ADCValue;
    //set the Multiplexer to MUXA and select the channel the sensor is connected to
    //The channels are predefined with global variables in this document
    AD1CHSbits.CH0SA = channel;
    Nop();
    //start manual sampling
    AD1CON1bits.SAMP = 1;
    //delay a short time to make sure its done sampling
    delayUs(1);
    //end manual sample
    AD1CON1bits.SAMP = 0;
    //wait until the conversion is done
    while(!AD1CON1bits.DONE){};
    //store the buffer results into the variable ADCValue
    ADCValue = ADC1BUF0;
    //output the value as an integer to be stored in a new variable when called
    return ADCValue;
}
//Example of use: int analogData = readAnalogChannel(STL);
//Samples the channel tied to the variable STL and stores the Buffer Data into analogData
/*This function is to allow the motor to slowly decelerate after the carrier tape*
 *is finished going through the system. The sensors will stop detecting proper   *
 *thresholds as soon as the tape is out of focus, so the motor should slowly ease*
 *out to make sure the tape is absolutely out of the system. Function accepts two*
 *parameters that allow a top acceleration and bottom acceleration to be defined *
 *the process uses the delay functions below to its advantage by increasing the  *
 *amount of time between steps to simulate the effect of deceleration.           */
void easeOut(int topaccel, int botaccel){
    //the top acceleration should be a smaller number than bottom acceleration
    //the for loop will use topaccel as the starting point, wait until it is 
    //greater than botaccel and loop whatever is inside the for until that happens
    for(topaccel; topaccel <= botaccel; topaccel++){
        //drive the motor with the changing topaccel number to simulate deceleration
        driveMotor(500, dir, topaccel);
        //small delay for next instruction to occur
        delayUs(1);
    }
}
//Example of use: easeOut(10, 20);
//This will bring the motor from using 100us between step instructions down to 200us
/*The function below is repeatable to drive the motor whatever way we want repeatedly*
 *the return type is void because we are not using any data that comes out, there are*
 *four parameters you can put values into. int step: how many steps do you want the  *
 *motor to take?, unsigned int direction: what direction do you want the motor to go?*
 *(0 drives it one way, 1 drives it the other way), int delayMicro: how many         *
 *multiples of 10us you want for the delay time between steps (less = faster, more   *
 * = slower) CAREFUL: going too fast will make the motor heat up quicker due to fast *
 *change in current!                                                                 */
void driveMotor(int step, unsigned int direction, int delayMicro){
  DIR_PIN = direction; //set direction of motor
  while(step >= 0){ //wait for steps to go all the way down to zero
    Nop();
    STEP_PIN = 0; 
    delayUs(delayMicro); //delay a multiple of 10us for quick transition
    Nop();
    STEP_PIN = 1; //going from low to high will step the motor
    delayUs(delayMicro);
    step--; //subtract one from steps each iteration
  }
}
//Example of use: driveMotor(50, 0, 20); 
//will set the number of steps to 50, drive the motor direction associated with logic LOW, and delay 200us between each change of logic
//The function below works the same way as delayMS, except it uses a multiple of 10us
void delayUs(int xUS){
  int c1;
  for(c1 = 0; c1 <= xUS;c1++){
    TMR1 = 0;
    T1CONbits.TON = 1;
    while(TMR1 < DELAYUS){};
    T1CONbits.TON = 0;
  }
}
//Example of use: delayUs(10);
//Will delay the instruction by 10 * 10us = 100us (microseconds)
/*The function below uses the built in Timer1 register to put some delay between code execution, *
 *there is an explanation at the top of this file how the number 10ms = 0x1388 using specific    *
 *values set by the register T1CON, the function returns no values and only delays the program   *
 *instructions by multiples of 10, because 10 is easier to deal with. Only one parameter is used:*
 *xMS, this defines how many multiples of 10ms you want (i.e. 1 will be 10ms, 5 will be 50ms, 10 *
 *will be 100ms, etc...) The method for timing sets a pre-defined variable called TMR1 to 0,     *
 *starts the timer, iterates waiting for TMR1 to reach DELAYMS x number of xMS times, and finally*
 *turn off the timer when finished. The function always makes sure that TMR1 is set back to 0 at *
 *appropriate times so we don't reference some random value and xMS.                             */
void delayMs(int xMS){
    int c1;
    //iterate the "wait" code xMS number of times
    for(c1 = 0; c1 <= xMS; c1++){
        //we really need to be sure TMR1 resets to 0 when it reaches the max we set
        TMR1 = 0;
        T1CONbits.TON = 1;
        //wait for TMR1 to reach the DELAY bit
        while(TMR1 < DELAYMS){};
        T1CONbits.TON = 0;
    }
}
//Example of use: delayMs(50);
//Will delay instruciton by 50 * 10ms, which is 500ms or half a second
