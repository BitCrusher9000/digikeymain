// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*******************************************************************************/
// DOM-IGNORE-END
// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************
/*  This section lists the other files that are included in this file.
 */
#include <stdint.h>
#include <xc.h>
#include "main.h"
#include <p24F16KL402.h>
void delayMs(unsigned int);
void delayUs(unsigned int);
/*
 Clock divider = Fosc/2 4Mhz
 Prescaler = 8
 Tick Counter Frequency = 8/4E6 = 2us
 Timer Count MS = 10ms/2us = 5000 = 0x1388
 Timer Count US = 10us/2us = 0x5
 */
//use defines for easier use
#define DELAY_MS 0x1388 //multiples of 10 are easier to use for delay
#define DELAY_US 0x5
#define SSWRITE LATAbits.LATA6
#define ENABLE LATAbits.LATA7
unsigned int spiWrite_Read(unsigned int);
void writeTMCL(unsigned int, unsigned int, unsigned int, unsigned int, unsigned int);
int main() 
{
    /*Reference clock for control, setup*/
    REFOCONbits.ROEN = 0; //disable the output first
    REFOCONbits.ROSEL = 0; //define the system clock (Fosc) 8Mhz WARNING: May not be able to drive Trinamic
    REFOCONbits.RODIV = 0000; //uses the exact Fosc as an output of 8Mhz
    REFOCONbits.ROEN = 1; //enable clock output on REFCLK
    /************************************/
    
    /*Define the inputs and outputs we use*/
    TRISAbits.TRISA6 = 0; //sets RA6 as output (SS, CS)
    TRISBbits.TRISB13 = 0; //set RB13 as output (SDO1)
    TRISBbits.TRISB11 = 0; //set RB11 as output (SCK1)
    TRISBbits.TRISB10 = 1; //set RB10 as input (SDI1)
    TRISAbits.TRISA7 = 0; //set RA7 as output (DIO0)
    /**************************************/
    
    /*Configure SPI for MODE1 (0 CKP, 0 CKE) and other setup modes*/
    SSP1CON1bits.SSPEN = 0; //disable first, then set configs
    SSP1STATbits.SMP = 1; //sample input data at end of data output
    SSP1STATbits.CKE = 0; //sets transmit from idle to active
    SSP1CON1bits.CKP = 0; //sets clock polarity to idle low
    SSP1CON1bits.SSPM = 0000; //sets clock to Fosc/2 which is 4Mhz
    SSP1CON1bits.SSPEN = 1; //enable serial ports after config
    /**************************************************************/
    
    /*Clear any pins that will not be used for serial communication*/
    PADCFG1bits.SCK2DIS = 1; //disable SCK2 as serial port
    PADCFG1bits.SDO2DIS = 1; //disable SDO2 as serial port
    PADCFG1bits.SCK1DIS = 0; //enable SCK1 as serial port
    PADCFG1bits.SDO1DIS = 0; //enable SDO1 as serial port
    /***************************************************************/
    
    /*Default initialization sets all AN labeled pins to analog, disable conflicting pins*/
    ANSBbits.ANSB15 = 0; //disable analog on RB15
    ANSBbits.ANSB14 = 0; //disable analog on RB14
    ANSBbits.ANSB13 = 0; //disable analog on RB13
    /*************************************************************************************/
    
    /*Configure the timer used for delay*/
    T1CONbits.TCS = 0; //sets timer frq to 4Mhz
    T1CONbits.TCKPS = 01; //sets tiemr prescaler to 8
    T1CONbits.TON = 0; //disable timer start for now
    /************************************/
    
    //Tell the slave not to listen, and enable the Trinamic Drive
    SSWRITE = 1;
    ENABLE = 0;
    
    //see function for details on how this works
    //sets appropriate configs of the Trinamic Chip TMC5130 and Rampmode for the driver
    writeTMCL(0x80, 0x00, 0x00, 0x00, 0x00); //GCONF
    
    writeTMCL(0xEC, 0x00, 0x01, 0x01, 0xD5); //CHOPCONF
    writeTMCL(0x90, 0x00, 0x07, 0x06, 0x03); //IHOLD_IRUN
    writeTMCL(0x91, 0x00, 0x00, 0x00, 0x0A); //TPWRDOWN = 10
    
    writeTMCL(0xF0, 0x00, 0x00, 0x00, 0x00); //PWMCONF
    
    writeTMCL(0xA4, 0x00, 0x00, 0x03, 0xE8); //A1 = 1000
    writeTMCL(0xA5, 0x00, 0x01, 0x86, 0xA0); //V1 = 100000
    writeTMCL(0xA6, 0x00, 0x00, 0xC3, 0x50); //AMAX = 50000
    writeTMCL(0xA7, 0x00, 0x01, 0x86, 0xA0); //VMAX = 100000
    writeTMCL(0xAA, 0x00, 0x00, 0x05, 0x78); //D1 = 1400
    writeTMCL(0xAB, 0x00, 0x00, 0x00, 0x0A); //VSTOP = 10
    
    writeTMCL(0xA0, 0x00, 0x00, 0x00, 0x00); //RAMPMODE = 0
    
    writeTMCL(0xA1, 0x00, 0x00, 0x00, 0x00); //XACTUAL = 0
    writeTMCL(0xAD, 0x00, 0x00, 0x00, 0x00); //XTARGET = 0
    
    //this is where intended motor movement happens with Positioning mode
    while(1){
        writeTMCL(0xAD, 0x00, 0x07, 0xD0, 0x00); //XTARGET = 512000
        delayMs(500);
        writeTMCL(0x21, 0x00, 0x00, 0x00, 0x00); //read 0x21 register on TMCL
        writeTMCL(0xAD, 0x00, 0x00, 0x00, 0x00); //XTARGET = 0
        delayMs(500);
        writeTMCL(0x21, 0x00, 0x00, 0x00, 0x00);
    }
    return 0;
}

void writeTMCL(unsigned int address, unsigned int byte1, unsigned int byte2, unsigned int byte3, unsigned int byte4){
    //put slight delay before sending data
    delayUs(1);
    //tell the slave to wake up and listen to data transfer
    SSWRITE = 0;
    //send the address first, all addresses are listed in the TMC5130 datasheet, 0x## is equivalent to a 8bit number
    //make sure to add 0x80 to the address if you want to write, leave the address as is for reading (datasheet tells what can be R/W)
    spiWrite_Read(address);
    //This is the actual data for the address to process, all bits must be filled
    //Even if the address has less than 40 bits, use 0x00 as filler data for unused spots
    //SPI sends MSB first, so empty data should be set before sending any actual data
    spiWrite_Read(byte1);
    spiWrite_Read(byte2);
    spiWrite_Read(byte3);
    spiWrite_Read(byte4);
    //data is finished after 40 bits (address + four bytes = 40 bits), tell the slave to stop listening
    SSWRITE = 1;
}

unsigned int spiWrite_Read(unsigned int data){
    //write to the buffer (activates SPI SCK)
    SSP1BUF = data;
    //wait until the buffer is full i.e. BF will equal 1 when full
    while(SSP1STATbits.BF == 0);
    //read immediately after the buffer is full
    return SSP1BUF;
}

void delayUs(unsigned int xUS){
    TMR1 = 0;
    T1CONbits.TON = 1;
    int c1;
    for(c1 = 0; c1 <= xUS; c1++){
        TMR1 = 0;
        while(TMR1 < DELAY_US);
    }
    T1CONbits.TON = 0;
}

void delayMs(unsigned int xMS){
    //make sure TMR1 is cleared
    TMR1 = 0;
    //Start the timer
    T1CONbits.TON = 1;
    int c1;
    //iterate the "wait" code xMS number of times
    for(c1 = 0; c1 <= xMS; c1++){
        //we really need to be sure TMR1 resets to 0 when it reaches the max we set
        TMR1 = 0;
        //wait for TMR1 to reach the DELAY bit
        while(TMR1 < DELAY_MS);
    }
    //timer should be turned off after all use is complete
    T1CONbits.TON = 0;
}
/*******************************************************************************
 End of File
*/