# CHANGELOG OF DIGI-KEY PROJECT CODE#
----------------------------------------------------------------------------
This document contains a summary of all the different versions of snippets 
created for the total program to be formed. These snippets will either be 
considered for final use or discarded if they do not function.

##VERSION 1.0##
----------------------------------------------------------------------------

###TITLE: SPI_EXPERIMENTS###
----------------------------------------------------------------------------

####SUMMARY OF SPI FUNCTION####
---------------------------------------------------------------------------------------------
The overall function was intended to work with the Trinamic Evaluation Board v1.5.
This code was designed to work with an interface called the Serial Peripheral Interface or 
SPI. SPI functions via four main wires: SCK, SDO SDI, and SS or CS. The SCK is the clock
signal which is a high frequency square wave of high and low values. This clock 
syncs the data that is sent on upswings or downswings of the clock. Data sent out 
is sent on the SDO or the MOSI line. This data is usually one byte long but some interfaces
are designed to handle longer pieces of information. Our system is designed to handle 8 bits 
of information. The SDI line or MISO line receives any data that is sent out from the slave.
The CS or SS (Chip Select or Slave Select line) is an active low signal that the Master 
controls. The slave will not listen to data until this line is specifically set to low.
---------------------------------------------------------------------------------------------
####CONFIGURATION BIT REGISTERS####
---------------------------------------------------------------------------------------------
The Microchip provided by the client has several registers that control different aspects.
Specifically, the PIC24F16KL402 has registers named for Master Syncnronous protocol.(MSSP)
Our device is capable of using I2C as well as SPI, so the control register contains both 
definitions under a master name of MSSP. The data sheet contains information on what the 
control registers of the SPI configuration are, there are outside references to other 
datasheets that contain extra information on how to use SPi or I2C properly. The external 
document provides code examples to modify for the system you work with. The document 
contains what control registers should be set and what kind of configuration someone should
use. This first version used a combination of these documents and online resources on how
to use SPI on Arduino.
---------------------------------------------------------------------------------------------
####SUMMARY OF PROGRAM FUNCTION####
---------------------------------------------------------------------------------------------
The Trinamic Evaluation Board only receives information in 40 bit format. The data is split 
into two types of data. The first byte contatins address information of control registers
on the TMCL 5130 board. The last four bytes contain the necessary data to set configuration
and control motor movement via various control registers. The board has Read, Read/Write, and 
Write Only registers marked R, R/W, W respectively in the data sheet. Reading registers does 
not require extra infromation to be added to the register address. If information is being
written, the value of 0x80 must be added to the address (0x80 is a MSB 1). Example would be 
control register 0x00, if you add 0x80 to 0x00 you get the address of 0x80 which tells the
Trinamic to write register 0x00. This format gives us a writing sequence for the Microchip.

The basic explanation of the main code in this snippet is that information is being sent 
one byte at a time to the Trinamic, once all 40 bits have been received the data is latched.
The trinamic can process what to do with the information based on the address provided with
Read or Write instructions. Once the information is processed, it completes the action.
After all configuration bits have been changed, the code continues on to the infinite while 
loop to repeat constantly. This code accesses Trinamic registers that are associated to a 
mode called Positioning mode. The positioning mode takes an initial position and final 
position and accelerates to the final position desired. Once a maximum velocity is reached, 
it can be coded to stay at that velocity for a number of secondds (given delay code, which
is implemented in this code). Once it is done moving to the position, you can tell the 
Trinamic to decelerate down to stopping. The code itself has very detailed comments to 
explain each aspect of the code.
---------------------------------------------------------------------------------------------
####PROBLEMS THAT AROSE####
---------------------------------------------------------------------------------------------
1. Desoldered resistor based on web info I found for an Arduino implementation, lost resistor
2. Burnt out two Microchips, one due to mismatching diagrams on the bridge and data sheet
and one due to an unknown short in wiring.
3. Could not get basic functioning code, even if the SPI code was working perfectly, tested 
with Saleae Digital Analyzer
---------------------------------------------------------------------------------------------
####POTENTIAL SOURCES OF ERROR####
---------------------------------------------------------------------------------------------
1. Human error in diagrams
2. Human error in depending on online sources
3. Human error in reading diagrams
4. Mismatching operating frequency, Trinamic 16Mhz, Microchip capable of 8Mhz maximum
5. Logic voltage level of Trinamic may not support 3.3V, couldn't find information
6. Complex wiring required, could have miswired, could have set programming parameters 
incorrectly
7. Incomplete testing
---------------------------------------------------------------------------------------------
####ADVANTAGES####
---------------------------------------------------------------------------------------------
1. More efficient
2. Does not get nearly as hot
3. Aesthetic
4. Uses standard communication method 
5. More industry freindly
6. Provided by company
---------------------------------------------------------------------------------------------
####DISADVANTAGES####
---------------------------------------------------------------------------------------------
1. Complex
2. Lots of wires
3. Bulky in size
4. Has frequency requirements
5. High frequency requires specialized wiring and filtering sometimes
---------------------------------------------------------------------------------------------
###END SPI_EXPERIMENTS###

---------------------------------------------------------------------------------------------
##END VERSION 1.0##

##VERSION 2.0##
---------------------------------------------------------------------------------------------
After more time was passing for the SPI version of the code without success, it was realized
a more simple solution could be achieved using simple digital motor drivers. We would have
spent more time figuring out how to get the SPI to work than desired.

###TITLE: SPARKFUN_DRIVER###
---------------------------------------------------------------------------------------------

####SUMMARY OF HOW THE SPARKFUN DRIVER FUNCTIONS####
---------------------------------------------------------------------------------------------
The driver in question only requires a external voltage for your motors and a connection to
a microprocessor. The pins need are labeled A1, A2, B1, B2, STEP, DIR, and +6...30VM. 
A1, A2, B1, B2 are the motor coil connections to the stepper motor being driven. The STEP
pin requires a change from HIGH to LOW for one step to occur on the stepper motor. The DIR pin
controls twhich direction the motor will spin, 0 sets it to go one way, 1 sets it to go the 
other direction. There are more pins on the board, but the ones listed here are the ones used.
---------------------------------------------------------------------------------------------
####SUMMARY OF PROGRAM FUNCTION####
---------------------------------------------------------------------------------------------
This program was designed to test the Sparkfun Easy Driver to see how easy it was to use.
The program introduces using delay for advatage of controlling the speed of the motor.
The delay is between changes of LOW to HIGH between steps on the STEP pin. The faster
you switch values, the smoother and faster the motor can go. There is maximum to how fast you 
can switch between values, so this must be considered. Also, the program introduces functions 
that include variable time between steps to simulate acceleration and deceleration. The functions
within the program drive the motor a number of steps given the multiple of delay provided and
what direction you want the motor to go in. The simulation of acceleration or deceleration is 
achieved by starting at a maximum or minimum delay and increasing or decreasing to a different
maximum or minimum delay.
---------------------------------------------------------------------------------------------
####PROBLEMS THAT AROSE####
---------------------------------------------------------------------------------------------
There were some programming errors and delay had to be recalculated because it was found
that the maximum frequency on the Microchip is actually 8 Mhz, not 32 Mhz. The data sheet
was a little unclear what the maximum was. The data sheet for the Microchip is very complicated
to begin with, so trying to decipher any type of program is difficult to begin with. 
---------------------------------------------------------------------------------------------
####POTENTIAL SOURCES FOR ERROR####
---------------------------------------------------------------------------------------------
There is no particular optimization of code, so there might be unregulated garbage data.
Also, the motor driver may fail to overheating or might cause problems for nearby parts.
This code does not seeem to contain any actual errors as it functions without much hitch.
---------------------------------------------------------------------------------------------
####ADVANTAGES####
---------------------------------------------------------------------------------------------
1. Small footprint
2. Very simple wiring
3. Ranged voltage logic level (3.3V or 5V)
4. Relatively simple programming
5. Intuitive board to use
---------------------------------------------------------------------------------------------
####DISADVANTAGES####
---------------------------------------------------------------------------------------------
1. Gets quite hot
2. Plenty of EMI emission
3. Obtained form Digi-Key but at a cost of 14 dollars
4. Might need a heat sync and fan
5. Possible short device life
---------------------------------------------------------------------------------------------
###END SPARKFUN_DRIVER###

---------------------------------------------------------------------------------------------
##END VERSION 2.0##

##VERSION 2.1##
---------------------------------------------------------------------------------------------
The next problem was approaching sensor data, this involved studying how analog functions
on the Microchip. 

###TITLE: ANALOG_PROBLEMS###
---------------------------------------------------------------------------------------------

####SUMMARY OF HOW ANALOG FUNCTIONS####
---------------------------------------------------------------------------------------------
The Microchip data sheet contains details on control registers relating to how the high speed
10bit ADC converter. The top of that section also referes to an oustide family reference doc
that provides more information on how to use Analog properly wiht examples of code. In general,
the ADC runs on the internal system clock and a ADC clock to convert variable analog data to 
a range of digital data in a few different 10-bit formats. The format we use is 10 bit integer.
The data depth is still 16 bits long, but only needs 10 to be accurate enough. The integer format
is structed as : (0000 00dd dddd dddd) where d is the data and the extra zeroes are the 16 bit 
container empty information (reserved for other formats). Data can be sampled manually or 
automatically, we decided to use manual sampling as it is slightly easier to understand.
the basic idea is to set analog inputs through TRIS and ANS registers first, then select
channnels to listen for analog data via AD1CHS. This method uses an amplifier called a sample and
hold amplifier. The positive reference is usually the analog pin you want to listen to while the
negative input is the internal voltage reference VR-. The data is then stored in a buffer
called ADC1BUF0 or BUF1 depending on which Multiplexer channel you are accessing.
---------------------------------------------------------------------------------------------
####SUMMARY OF PROGRAM FUNCTION####
---------------------------------------------------------------------------------------------
The overall idea was to get the motor driver to respond to a digital value provded by the 
ADC conversion. The program sets up proper configuration, then does a manual sample in the first
few linees of the main code in an infinte while loop. Then the data is passed from the buffer
and used in a logic statement to get the motor to move based on the threshold. This simple code
gave important roots to define our final program with. A function was added later on to 
sample Analog data from multiple sensors to compare and provide logic conditions for. Will be 
briefly explained in the second to final draft of the final code. (Version 2.2 most likely)
---------------------------------------------------------------------------------------------
####PROBLEMS THAT AROSE####
---------------------------------------------------------------------------------------------
There was a critical error in the program that was difficult to find, it kept crahsing my 
program over and over with no errors passed. I had ended up putting an incorrect variable
reference in one of my functions, it was not caught by the compiler and provided a mind
twisting failure that could not be determined easily. Also, it was difficult to even 
adapt code from the extra document found because it was an older system. Almost all the systems
on this chip are difficult to understand and require plenty of testing and troubleshooting.
---------------------------------------------------------------------------------------------
####POTENTIAL SOURCES FOR ERROR####
---------------------------------------------------------------------------------------------
1. Using one buffer might be more difficult than imagined
2. Multiple sensors might cause feedback for other sensors
3. Interrupts may occur if data is achieved at the wrong time
4. Incorrect wiring can lead to troubleshooting
5. Programming logic might lead to unreached statements
---------------------------------------------------------------------------------------------
####ADVANTAGES####
---------------------------------------------------------------------------------------------
1. Still relatively simple code
2. Does not require too many wires
3. Sensors are very sensitive and easy to test
4. Lower frequency ranges may lead to less problems
5. Provides learning tool for ADC
---------------------------------------------------------------------------------------------
####DISADVANTAGES####
---------------------------------------------------------------------------------------------
1. Requires plenty of technical information to understand
2. Wires are still going to be a little messy
3. Sensor feedback error
4. Invisible light 
5. May be difficult to program for multiple sensors beyond 2
---------------------------------------------------------------------------------------------
###END ANALOG_PROBLEMS###

---------------------------------------------------------------------------------------------
##END VERSION 2.1##

##VERSION 2.2##
---------------------------------------------------------------------------------------------
The latest version of code incorporates the full logic tree including the start threshold
to make sure that the system does not read random values at the beginning. 

###TITLE: FULL_LOGIC_TEST###
---------------------------------------------------------------------------------------------

####SUMMARY OF PROGRAM FUNCTION####
---------------------------------------------------------------------------------------------
The analog function still works the same way compared to Version 2.1, but this time the analog
values are compared together in pairs. The top left and bottom left sensors are pairs while 
the bottom right and top right sensors are pairs. We only need to compare these pairs in 
reference to the opposite side of the same pair (compare the same sensors on the same side)
The logic tree follows the assumption that a minimum threshold is tested by the top left 
sensor, then it checks if the tape is in the correct postion by comparing the top left and
bottom left sensors, if that condition is met, the correct light will turn on and the motor
will move. If the case is not met, then it moves on to testing different sensors for different
positions. Specifically, the top right and bottom right sensors are compared to check if the
tape has been fed in with the wrong end first. If this is met, the Wrong End LED lights, if 
it is not met, then it moves on to checking the inverse of the previous condition is checked
for the tape being upside down. If this condition is met, the Upside Down light activates, 
if not met, the final combination of both Upside Down and Wrong end LED's activate. 
---------------------------------------------------------------------------------------------
####PROBLEMS THAT AROSE####
---------------------------------------------------------------------------------------------
There are some calibration issues with the distance of sensors and the housing design. The 
improper width and height are causing fluctuations in the sensor readings, therefor causing
multiple lights to blink. The upside down conditions are met without issue, but the right side
up conditions are a little buggy. 
---------------------------------------------------------------------------------------------
####POTENTIAL SOURCES OF ERROR####
---------------------------------------------------------------------------------------------
1. Improper threshold checking in programming
2. Callibration issues with design 
3. Potential mixed signals from conflicting sensors
4. Curvature of the tape causing error in reading
5. Tape reflectivity is not constant
6. Error from holes and parts in carrier tape
---------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------
###END FULL_LOGIC_TEST###

---------------------------------------------------------------------------------------------
##END VERSION 2.2##

---------------------------------------------------------------------------------------------
#END CHANGELOG#
